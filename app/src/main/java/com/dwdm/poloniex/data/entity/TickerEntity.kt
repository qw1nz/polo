package com.dwdm.poloniex.data.entity

import com.google.gson.annotations.SerializedName

data class TickerEntity(
        @SerializedName("id") val id: Int?,
        @SerializedName("last") val last: String?,
        @SerializedName("lowestAsk") val lowestAsk: String?,
        @SerializedName("highestBid") val highestBid: String?,
        @SerializedName("percentChange") val percentChange: String?,
        @SerializedName("baseVolume") val baseVolume: String?,
        @SerializedName("quoteVolume") val quoteVolume: String?,
        @SerializedName("isFrozen") val isFrozen: String?,
        @SerializedName("high24hr") val high24hr: String?,
        @SerializedName("low24hr") val low24hr: String?
)
