package com.dwdm.poloniex.data

import com.dwdm.poloniex.data.entity.TickerEntity
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiService {
    @GET("public?command=returnTicker")
    fun returnTicker(): Observable<HashMap<String, TickerEntity>>

    companion object {
        const val BASE_URL = "https://poloniex.com/"
    }
}