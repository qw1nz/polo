package com.dwdm.poloniex.view.ticker

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.dwdm.poloniex.R
import com.dwdm.poloniex.view.ticker.model.TickerItem

class TickerVH(view: View) : RecyclerView.ViewHolder(view) {

    private val name: TextView = view.findViewById(R.id.name)
    private val last: TextView = view.findViewById(R.id.last)
    private val highestBid: TextView = view.findViewById(R.id.highestBid)
    private val percentChange: TextView = view.findViewById(R.id.percentChange)

    fun bind(tickerItem: TickerItem) {
        name.text = itemView.context.getString(R.string.name_label, tickerItem.name)
        last.text = itemView.context.getString(R.string.last_label, tickerItem.last)
        highestBid.text = itemView.context.getString(R.string.highestbid_label, tickerItem.highestBid)
        percentChange.text = itemView.context.getString(R.string.percentchange_label, tickerItem.percentChange)
    }

}