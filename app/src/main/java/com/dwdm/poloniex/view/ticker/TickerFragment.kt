package com.dwdm.poloniex.view.ticker

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.dwdm.poloniex.R
import com.dwdm.poloniex.domain.TickerCommand
import com.dwdm.poloniex.domain.TickerMapper
import com.dwdm.poloniex.view.PApp
import com.dwdm.poloniex.view.base.BaseFragment
import etr.android.reamp.mvp.ReampPresenter
import kotlinx.android.synthetic.main.fragment_ticker.*

class TickerFragment : BaseFragment<TickerPresenter, TickerStateModel>() {

    private val adapter: TickerAdapter = TickerAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_ticker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ticker_recycler_view.adapter = adapter
        ticker_recycler_view.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }


    override fun onStateChanged(stateModel: TickerStateModel) {
        if (stateModel.error.isNullOrEmpty()) {
            adapter.setItems(stateModel.items)
        } else {
            Toast.makeText(context, stateModel.error, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }

    override fun onCreateStateModel(): TickerStateModel {
        return TickerStateModel()
    }


    override fun onCreatePresenter(): ReampPresenter<TickerStateModel> {
        val app = activity?.application as PApp

        return TickerPresenter(TickerCommand(app.apiService, TickerMapper()))
    }

    fun active() {
        presenter.active()
    }

    fun inactive() {
        presenter.inactive()
    }


    companion object {
        const val TAG = "tag.TickerFragment"
    }

}

