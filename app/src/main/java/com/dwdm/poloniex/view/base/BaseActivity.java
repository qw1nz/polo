package com.dwdm.poloniex.view.base;

import etr.android.reamp.mvp.ReampAppCompatActivity;

abstract public class BaseActivity<PRESENTER extends BasePresenter<STATE_MODEL>, STATE_MODEL extends BaseStateModel>
        extends ReampAppCompatActivity<PRESENTER, STATE_MODEL> {
}
