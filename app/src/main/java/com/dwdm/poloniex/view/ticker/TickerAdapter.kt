package com.dwdm.poloniex.view.ticker

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dwdm.poloniex.R
import com.dwdm.poloniex.view.ticker.model.TickerItem

class TickerAdapter : RecyclerView.Adapter<TickerVH>() {

    init {
        setHasStableIds(true)
    }

    private val items = mutableListOf<TickerItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TickerVH {
        return TickerVH(LayoutInflater.from(parent.context).inflate(R.layout.item_ticker, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: TickerVH, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.hashCode().toLong()
    }

    fun setItems(items: List<TickerItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

}