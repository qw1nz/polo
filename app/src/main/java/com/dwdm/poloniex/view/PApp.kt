package com.dwdm.poloniex.view

import android.app.Application
import com.dwdm.poloniex.data.ApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class PApp : Application() {
    lateinit var apiService: ApiService

    override fun onCreate() {
        super.onCreate()
        initApiService()
    }

    private fun initApiService() {
        apiService = Retrofit.Builder()
                .baseUrl(ApiService.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                        OkHttpClient
                                .Builder()
                                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                                .build()
                )
                .build()
                .create(ApiService::class.java)
    }
}