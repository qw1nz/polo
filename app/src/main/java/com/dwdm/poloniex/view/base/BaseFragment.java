package com.dwdm.poloniex.view.base;

import etr.android.reamp.mvp.ReampFragment;

abstract public class BaseFragment<PRESENTER extends BasePresenter<STATE_MODEL>, STATE_MODEL extends BaseStateModel>
        extends ReampFragment<PRESENTER, STATE_MODEL> {
}
