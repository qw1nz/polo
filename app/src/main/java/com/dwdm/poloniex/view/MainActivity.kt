package com.dwdm.poloniex.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dwdm.poloniex.R
import com.dwdm.poloniex.view.ticker.TickerFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pager.adapter = PagerAdapter(supportFragmentManager)
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                if (position == TICKER_INDEX) {
                    findTickerFragment()?.active()
                } else {
                    findTickerFragment()?.inactive()
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }

    private fun findTickerFragment(): TickerFragment? {
        supportFragmentManager.fragments.forEach { f ->
            if (f is TickerFragment) {
                return f
            }
        }
        return null
    }

    private inner class PagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            if (position == TICKER_INDEX) {
                return TickerFragment()
            }
            return EmptyFragment()
        }

        override fun getCount(): Int {
            return PAGES_COUNT
        }

        override fun getPageTitle(position: Int): CharSequence? {
            if (position == TICKER_INDEX) {
                return this@MainActivity.getString(R.string.title_ticker)
            }
            return this@MainActivity.getString(R.string.title_empty)
        }
    }

    companion object {
        const val TICKER_INDEX = 0
        const val PAGES_COUNT = 2
    }
}

class EmptyFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_empty, container, false)
    }
}
