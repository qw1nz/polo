package com.dwdm.poloniex.view.ticker

import com.dwdm.poloniex.view.base.BaseStateModel
import com.dwdm.poloniex.view.ticker.model.TickerItem

class TickerStateModel : BaseStateModel() {
    var items: List<TickerItem> = mutableListOf()
    var error: String? = null
}