package com.dwdm.poloniex.view.ticker.model

import java.io.Serializable

data class TickerItem(
        val id: Int,
        val name: String,
        val last: String,
        val highestBid: String,
        val percentChange: String
) : Serializable