package com.dwdm.poloniex.view.ticker

import android.util.Log
import com.dwdm.poloniex.domain.TickerCommand
import com.dwdm.poloniex.view.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class TickerPresenter(private val tickerCommand: TickerCommand) : BasePresenter<TickerStateModel>() {

    private var disposable: Disposable? = null

    private fun parseError(error: Throwable?): String {
        return error?.localizedMessage ?: ""
    }

    fun onResume() {
        subscribeToTickerUpdates()
    }

    fun onPause() {
        unsubscriveFromTickerUpdates()
    }

    fun active() {
        subscribeToTickerUpdates()
    }

    fun inactive() {
        unsubscriveFromTickerUpdates()
    }

    private fun subscribeToTickerUpdates() {
        disposable = tickerCommand
                .getTickerHot()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            stateModel.items = it
                            sendStateModel()
                        },
                        {
                            stateModel.error = parseError(it)
                            sendStateModel()
                        }
                )
    }

    private fun unsubscriveFromTickerUpdates() {
        disposable?.dispose()
    }

}

