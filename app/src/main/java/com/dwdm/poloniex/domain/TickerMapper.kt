package com.dwdm.poloniex.domain

import com.dwdm.poloniex.data.entity.TickerEntity
import com.dwdm.poloniex.view.ticker.model.TickerItem

class TickerMapper {
    fun toTickerItem(name: String?, tickerEntity: TickerEntity?): TickerItem? {
        tickerEntity ?: return null
        name ?: return null

        return TickerItem(
                id = tickerEntity.id ?: -1,
                name = name,
                highestBid = tickerEntity.highestBid ?: "",
                last = tickerEntity.last ?: "",
                percentChange = tickerEntity.percentChange ?: ""
        )
    }

}