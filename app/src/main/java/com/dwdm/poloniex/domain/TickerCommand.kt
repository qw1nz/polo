package com.dwdm.poloniex.domain

import com.dwdm.poloniex.data.ApiService
import com.dwdm.poloniex.view.ticker.model.TickerItem
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class TickerCommand(
        private val apiService: ApiService,
        private val mapper: TickerMapper
) {
    private val pooledScheduler = Schedulers.from(Executors.newFixedThreadPool(2) )

    fun getTickerHot(): Observable<MutableList<TickerItem>> {
        return Observable
                .interval(0,5, TimeUnit.SECONDS)
                .observeOn(Schedulers.io())
                .flatMap { apiService.returnTicker() }
                .map { tickerEntities ->
                    mutableListOf<TickerItem>().apply {
                        tickerEntities.keys.forEach { key ->
                            val tickerItem = mapper.toTickerItem(key, tickerEntities[key])
                            if (tickerItem != null) {
                                add(tickerItem)
                            }
                        }
                    }
                }
                .subscribeOn(pooledScheduler)
    }
}

